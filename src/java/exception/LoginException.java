package exception;

public class LoginException extends RuntimeException {

    public static final long serialVersionID = 1L;

    public LoginException(String msg) {
        super(msg);

    }
}
