package application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Logar {

   /* public static boolean Usuarios(String usuario) {

        List<String> users = new ArrayList<>();
        String local = "/home/tesla/NetBeansProjects/WebApplication/Users.txt";
        try (BufferedReader br = new BufferedReader(new FileReader(local))) {

            while (br.ready()) {

                users.add(br.readLine());

            }
            boolean x = false;
            for (int i = 1; i < users.size(); i++) {
                //x = users.get(i).equals(usuario);
                x = users.contains(usuario);
            }

            if (x == true) {
                return true;
            } else if (usuario == null) {
                throw new IllegalAccessError("");

            } else {

                throw new IllegalAccessError("Erro: Usuario Inválido.");

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Logar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Logar.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;

    }
*/
    public static boolean validarSenha(String senha) throws IOException {

        if (senha.length() == 8) {
            if (senha.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}")) {
                return true;
            }
        } else {
            throw new IllegalArgumentException("Erro: A senha deve conter 8 caracteres.");

        }
        return false;
    }

}
