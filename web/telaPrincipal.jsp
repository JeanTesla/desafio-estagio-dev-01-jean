<%-- 
    Document   : telaPrincipal
    Created on : 22/03/2019, 18:27:18
    Author     : tesla
--%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tela Principal</title>
    </head>
    <body>

        <% String usuario = (String) session.getAttribute("usuario");
            String TipoLogin = (String) session.getAttribute("TipoLogin");
            char resp = Character.MIN_VALUE;
            if (usuario == null) {
                response.sendRedirect("index.jsp");
            } else if (TipoLogin.equals("op1")) {
                resp = 'a';
            } else {
                resp = 'c';
            }

            switch (resp) {
                case 'a':
                    out.println("Meu nome � "+usuario+".");
                    out.print("<p>Eu sou usu�rio administrador do sistema.</p>"); 
                    break;
                case 'c':
                    out.println("Meu nome � "+usuario+".");
                    out.print("<p>Eu sou usu�rio comum do sistema.</p>"); 
                    break;
            }
        %>

        <br/><br/> <a href="Deslogar.jsp">Deslogar</a>
    </body>
</html>
