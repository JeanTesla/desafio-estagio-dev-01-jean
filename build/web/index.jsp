<%-- 
    Document   : TelaLogin
    Created on : 22/03/2019, 16:15:49
    Author     : tesla
--%>



<%@page import="application.Logar"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tela Login</title>
    </head>
    <body>
        <form action="index.jsp" method="post">
            Login: <br/><input type="text" name="usuario"/><br/>
            Senha: <br/><input type="password" name="senha"/><br/>
            Tipo de usuário:
            <input type="RADIO" name="TipoLogin" value="op1">Admin 
            <input type="RADIO" name="TipoLogin" value="op2">Comum
            <br/><br/><input type="submit" value="logar"/><br/>

        </form>

        <%  try {
                String usuario = request.getParameter("usuario");
                String senha = request.getParameter("senha");
                String TipoLogin = request.getParameter("TipoLogin");
                

                if ((usuario != null) && (Logar.validarSenha(senha) == true)
                        && (!usuario.isEmpty()) && (!senha.isEmpty())) {

                    session.setAttribute("usuario", usuario);
                    session.setAttribute("TipoLogin", TipoLogin);
                    response.sendRedirect("telaPrincipal.jsp");
                }
            } catch (IOException e) {
                throw new exception.LoginException(e.getMessage());
            } catch (IllegalArgumentException e) {
                out.print(e.getMessage());
            } catch (IllegalAccessError e) {
                out.print(e.getMessage());
            }
        %>

    </body>

</html>
